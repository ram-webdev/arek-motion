@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="container-fluid">
        <div class="row gbr">
            <div class="col-lg-7">
                    <div class="logo"></div>
                    <div class="tulisan">
                        <p class="judul">Aplikasi Rekruitasi </p>
                        <p class="motion">Mobile Innovation Laboratory</p>
                        <p class="tahun"><?php echo date("Y");?></p>
                        <div class="lbr_grs grs">
                        </div>
                    </div>
            </div>
            <div class="col-lg-5 formlogin">
                    <nav class="navbar navbar-expand-lg navbar-light">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                              <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                              <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a href="#" href="#" class="nav-link">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" onClick="return check()" class="nav-link">Registration Info</a>
                                </li>
                                <li class="nav-item"><a href="http://line.me/ti/p/~@biy7493e" class="nav-link">Contact</a></li>
                                <li class="nav-item geserdikit mt-md-2 mt-4" style="margin-left: 0px !important;" class="nav-link">
                                    <a style="margin-left: 0px !important;" href="#">Sign Up</a>
                                </li>
                              </ul>
                            </div>
                          </nav>
                <div class="row">
                    <div class="col-md-8 offset-md-3 " style="margin-top: 15%;">
                        <p class="signin">Sign In</p>
                        <div class="grs_sign"></div>
                        {{-- <div class="alert" role="alert"> 
                            Alert 
                        </div>--}}
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                        <div class="form-group" >
                            <input id="email" type="text" class="form-control" name="username" value="{{ old('email') }}" placeholder="Username" required autofocus>
                        </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="pwd" name="password" placeholder="Password" required>
                            </div>
                            <button type="submit" class="btn btn-danger btn-lg btn-block tombol-login">Sign In</button>
                            @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
    <script type='text/javascript'>

    function check()
    {
        alert("Silahkan hubungi kontak kita ya kak :)");
    }
    
    </script>
    
@endsection
